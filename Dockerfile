FROM alisw/slc6-builder
RUN groupadd -g 5000 aliprod &&                                                                            \
    useradd -u 5000 -g 5000 -m -d /var/lib/aliprod aliprod &&                                              \
    pip install pika &&                                                                                    \
    curl -Lo /usr/bin/thyme_worker https://raw.githubusercontent.com/dberzano/thyme/master/thyme_worker && \
    chmod 0755 /usr/bin/thyme_worker
RUN rpm --rebuilddb && yum clean all && yum install -y attr
