ALICE HLT Plancton configuration
================================

Configuration for [Plancton](https://github.com/mconcas/plancton) for the ALICE
HLT cluster.